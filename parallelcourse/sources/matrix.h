#ifndef _matrix_h_
#define _matrix_h_

#include <cstddef>
#include <iostream>
#include <vector>
#include <random>
#include <cassert>
#include <atomic>

using namespace std;

class matrix{
public:
	matrix(){};
	matrix(size_t n);
	~matrix(){};

	size_t size() const {return _size;};
	void randomize();
	void transpose();

	friend istream& operator>>(istream& in, matrix& m);
	friend ostream& operator<<(ostream& out, const matrix& m);
	friend matrix operator*(matrix& a, matrix& b);
	friend bool operator==(const matrix& a, const matrix& b);

private:
	size_t _size{};
	vector<vector<double>> _data{};
	
};

#endif