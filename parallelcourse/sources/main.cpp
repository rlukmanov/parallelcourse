#include <iostream>
#include <vector>
#include <chrono>
#include <fstream>
#include <cassert>
#include "matrix.h"

using namespace std;

int main(int argc, char ** argv){
	
	int n;

	if (argc > 1){
		sscanf(argv[1], "%d", &n);
	}
	else {

		ifstream in("./test/matrix.in");
		matrix a(16);
		in >> a;

		a = a * a;

		ifstream out("./test/matrix.out");
		matrix b(16);
		out >> b;
		
		if (a == b) cout << "> Equal" << endl;
		else cout << "> Not equal " << endl;

		return 0;
	}

	matrix a(n), b(n), c(n);

	a.randomize();
	b.randomize();

	auto start = chrono::steady_clock::now();
	c = a * b;
	auto end = chrono::steady_clock::now();

	auto diff = end - start;
	int time = chrono::duration_cast<chrono::milliseconds>(diff).count();
	cout << time << " ms" << endl;

	return 0;
}