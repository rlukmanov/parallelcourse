#ifdef ENABLE_OPENMP
#include <omp.h>
#endif

#ifdef ENABLE_THREADS
#include <thread>
#define NTHREADS 8
#endif


#include "matrix.h"

std::atomic<size_t> atomic_shared_row = {};

matrix::matrix(size_t n){
	_data.resize(n);
	for (auto & elem: _data){
		elem.resize(n, 0);
	}
	_size = n;
}

void matrix::randomize(){
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> uid(-1., 0.);
	for (auto & row: _data){
		for (auto & elem: row){
			elem = uid(gen);
		}
	}
}

void matrix::transpose(){
	int n = _size;

	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
			swap(_data[i][j], _data[j][i]);
		}
	}
}

istream& operator>>(istream& in, matrix& m){
	bool first_row = true;
	char c;
	in >> c; assert(c == '{');
	for (size_t row = 0; row != m._size; ++row){
		if (first_row) first_row = false;
		else {
			in >> c; assert(c == ',');
		}
		bool first_elem = true;
		in >> c; assert(c == '{');
		for (size_t col = 0; col != m._size; ++col){
			if (first_elem) first_elem = false;
			else{
				in >> c; assert(c == ',');
			}
			in >> m._data[row][col];
		}
		in >> c; assert(c == '}');
	}
	in >> c; assert(c == '}');
	return in;
}

ostream& operator<<(ostream& out, const matrix& m){
	bool first_row = true;
	std::cout << endl << '{';
	for (const auto &row: m._data){
		if (first_row) first_row = false; else std::cout << ',' << std::endl;
		bool first_elem = true;
		std::cout << '{';
		for (const auto &elem: row){
			if (first_elem) first_elem = false; else std::cout << ", ";
			std::cout << elem;
		}
		std::cout << '}';
	}
	std::cout << '}' << endl;
	return out;
}

matrix operator*(matrix& a, matrix& b){
	unsigned n = a.size();
	matrix res(n);

	matrix d = b;
	d.transpose();

	#ifdef ENABLE_OPENMP
		omp_set_num_threads(8);
	#pragma omp parallel
	{
	#pragma omp for
	#endif

	#ifdef ENABLE_THREADS
		std::array<std::thread,NTHREADS> threads;
		for (unsigned nthreads = 0; nthreads != NTHREADS; ++nthreads){
			threads[nthreads] = std::thread([](matrix& a, matrix& d, matrix& res) -> void 
				{
					size_t n = a.size();
					while (true){
						size_t local_row = atomic_shared_row++;
						if (local_row >= n) break;
						for (size_t i = 0; i < n; ++i){
							for (size_t p = 0; p < n; ++p){
								res._data[local_row][i] += a._data[local_row][p] * d._data[i][p];
							}
						}
					}
				}
				, std::ref(a), std::ref(d), std::ref(res));

		}
		for (unsigned nthreads = 0; nthreads != NTHREADS; ++nthreads){
			threads[nthreads].join();
		}
	#else
		for (unsigned i = 0; i < n; ++i){
			for (unsigned j = 0; j < n; ++j){
				for (unsigned k = 0; k < n; ++k){
					res._data[i][j] += a._data[i][k] * d._data[j][k];
				}
			}
		}
	#endif
	#ifdef ENABLE_OPENMP
	}
	#endif
	return res;
}

bool operator==(const matrix& a, const matrix& b){
	int n = a._size;
	if (a._size != b._size) return 0;
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < n; ++j){
			if (fabs(a._data[i][j] - b._data[i][j]) > 1E-5) return 0;
		}
	}
	return 1;
}